const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");

const app = express();
app.use(cors({ origin: "*" }));
app.use(bodyParser.json());

app.listen(3000, () => {
  console.log("The server started on port 3000 !!!!!!");
});

app.get("/", (req, res) => {
  res.send(
    "<h1 style='text-align: center'>Serveur Email</h1>"
  );
});

app.post("/sendmail", (req, res) => {
  console.log("request came");
  let user = req.body;
  sendMail(user, info => {
    console.log(`email send`);
    res.send(info);
  });
});


async function sendMail(user, callback) {
  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD
    }
  });

  let mailOptions = {
    from: '"Doctolib"<admin.doctolib.com>',
    to: user.email,
    subject: "Demande d'ouverture de compte validée",
    html: `<h1>Bonjour ${user.name}</h1><br>
    <h4>Votre demande d'ouverture est validée vous pouvez vous connecter</h4>
    <p>Votre mot de passe temporaire est ${user.password}</p>`
  };

  let info = await transporter.sendMail(mailOptions);

  callback(info);
}

